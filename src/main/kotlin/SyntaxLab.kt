import kotlin.system.exitProcess

var funds: Double = 100.0
val pswd = "password"

fun deposit(amount: Double): Unit {
    funds += amount
    println("Balance after deposit is $funds")
}

fun main() {
    var input: String
    var cmd: List<String>

    while (true) {
        print("Command: ")
        input = readLine()!!
        cmd = input.split(" ")
        when (cmd[0]) {
            // Each command goes here...
            "balance" -> balance()
            "withdraw" -> {
                try {
                    withdraw(cmd[1].toDouble())
                } catch (ex: Exception) {
                    handleOperandExceptions(ex)
                }
            }
            "deposit" -> {
                try {
                    deposit(cmd[1].toDouble())
                } catch (ex: Exception) {
                    handleOperandExceptions(ex)
                }
            }
            "exit" -> {
                println("Exiting...")
                exitProcess(0)
            }

            else -> println("Invalid command")
        }
    }
}

private fun handleOperandExceptions(ex: Exception) {
    when (ex) {
        is IndexOutOfBoundsException -> println("Invalid operand. Command must be followed by an amount")
        is NumberFormatException -> println("Invalid amount specified")
    }
}


fun withdraw(amount: Double): Unit {
    var input: String
    while (true) {
        print("Enter password: ")
        input = readLine()!!

        if (input == pswd) {
            if (amount > funds) {
                println("Insufficient balance")
                break
            }
            funds -= amount
            println("$amount has been withdrawn successfully")
            println("Balance is $funds")
            break
        } else println("Wrong password, try again")
    }
}

fun balance(): Unit {
    println("Current balance: $funds")
}

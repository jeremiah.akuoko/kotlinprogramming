fun main() {
    println("Hello, World!")
    var myString = "Hello!"

    println(myString)

    // line formula:  y = 3x - 1 for x [-5..5]
    for (x in -5..5) {
        println("y = 3($x) - 1 = ${calculateY(3, x, -1)}")
    }

    // y = 3x + 5
    // y = 4x + 2
    var y1 = 0;
    var y2 = 0;
    for (x in 0..20) {
        y1 = calculateY(3, x, 5)
        y2 = calculateY(4, x, 2)
        if (y1 == y2) {
            println("y = 3x + 5 and y = 4x + 2 intersect at ($x,$y1)")
        }
    }

    // y = 3x + 5 and y = 4x + 2
    // y = 2x + 1 and y = 3x + 3
    // y = -2x + 3 and y = 3x - 2
    println("\ny = 3x + 5 and y = 4x + 2")
    intersection(3, 5, 4, 2)
    println("\ny = 2x + 1 and y = 3x + 3")
    intersection(2, 1, 3, 3)
    println("\ny = -2x + 3 and y = 3x - 2")
    intersection(-2, 3, 3, -2)
}

fun calculateY(m: Int, x: Int, c: Int): Int {
    // y = mx + c - formula for a straight line
    return m * x + c
}

fun intersection(mOne: Int, cOne: Int, mTwo: Int, cTwo: Int): Unit {
    // This function takes the equations of two lines:
    // yOne = mOne * x + cOne and yTwo = mTwo * x + cTwo
    // and prints the X value they intersect at
    // or prints "Don't intersect" if they do not.
    // You only need to check x values between 0 and 100
    var intersectsAt: Int? = null
    for (x in 0..20) {
        if (calculateY(mOne, x, cOne) == calculateY(mTwo, x, cTwo)) {
            intersectsAt = x;
            break
        }
    }

    if (intersectsAt != null)
        println("Lines intersect at X=$intersectsAt")
    else println("Don't intersect")
}